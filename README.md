# Recherche Web : Domaine NLU

Obtenez des réponses résumées à partir du Web.

> ** DISCLAMER : ** Je ne suis en aucun cas, forme ou condition, capable de contrôle sur la sortie produite. Ne prenez aucune réponse pour acquise.

```
-> fait une recherche sur le net
Que souhaitez-vous rechercher ?
->  Quand la deuxième guerre mondiale se termine-t-elle?
8 mai 1945,
->  Quand est mort Albert Einstein ?
1955, # En anglais on a la date complète...
->  Qui était la mère d'Albert Einstein ?
pauline koch # Bien joué, en anglais il trouve pas le nom de jeune fille
->  A quelle distance est la lune ?
384 400 km
->  Quelle est la taille d'Emmanuel Macron ?
[taille;1m73] # Des progrès à faire sur la mise en page...
-> Est-ce que la lune brille ?
pourquoi la lune brille si elle n'émet pas sa propre lumière # Sa première question meta-physique :O
-> Où est la Suisse ?
la suisse figure parmi les pays les plus "propres" au monde # C'est pas vraiment ce que j'ai demandé...
->  Quelle est la hauteur du mont Everest ?
8 849 mètres.
->
```

## Dépendances

Vous devez installer la bonne version de `torch` pour votre système.

Consulter le [manuel d'utilisation de `pytorch`](https://pytorch.org/TensorRT/tutorials/installation.html) pour plus d'information.

Consulter le fichier `requirements.txt` pour un aperçu complet des dépendances de ce domaine.

## Installation

Utilisez l'[Outil de Gestion de Domaines](https://gitlab.com/waser-technologies/technologies/dmt) (DMT en anglais) pour installer, valider et entraîner sans effort un nouveau modèle de language naturel utilisant ce domaine.

```zsh
dmt -V -T -L fr -a https://gitlab.com/waser-technologies/data/nlu/fr/web-search.git
```

## Utilisation

Pour tester le modèle sur ce domaine, utilisez le `dmt` pour servir le dernier modèle.

```zsh
dmt -S --lang fr
```
Cela prendra un certain temps à charger.

Une fois que vous voyez:
> `INFO     root  - Rasa server is up and running.`

Vous pouvez interroger le serveur dans un autre terminal.

```zsh
curl -XPOST localhost:5005/webhooks/rest/webhook -s -d '{ "message": "a quelle distance est la lune", "sender": "user" }'
```

Ou utilisez simplement [`Assistant`](https://gitlab.com/waser-technologies/technologies/assistant).

```zsh
assistant À quelle distance est la lune?
```

## Comment ça marche?

Ce domaine réalise une recherche sur le Web à partir de requêtes qui seraient considérées comme hors de portée en utilisant deux composants principaux :
- un formulaire de recherche rasa pour demander :
    - une `requête`
    - un `moteur_de_recherche`
      (par défaut `google` - `bing`, `duckduckgo`, `ask` -)
    
    Lorsque l'utilisateur demande d'effectuer une recherche sans fournir ces créneaux.
    
    Cela déclenchera une action de recherche personnalisée.
- une intention de recherche pour déclencher directement :
    - l'action de recherche personnalisée
    
    Lorsque l'utilisateur pose une requête hors de portée que seule une recherche sur le Web peut y répondre.

Si la requête est trop longue, utilise [`plguillou/t5-base-fr-sum-cnndm`](https://huggingface.co/plguillou/t5-base-fr-sum-cnndm) pour résumer la requête à rechercher.

Prend les trois premiers résultats, en saisit le contenu textuel et utilise [`etalab-ia/camembert-base-squadFR-fquad-piaf`](https://huggingface.co/etalab-ia/camembert-base-squadFR-fquad-piaf) pour répondre à la requête compte tenu du contenu des résultats de la recherche.

## Existe-t-il des modèles pré-entraînés de ce domaine ?

Oui, j'entraîne et distribue des modèles en utilisant tous les [domaines que j'ai rendus publics](https://gitlab.com/waser-technologies/data/nlu) pour une langue particulière.

Vous pouvez télécharger les derniers modèles pré-entraînés pour la compréhension du language naturel en français sous [Modèles/Français/NLU](https://gitlab.com/waser-technologies/models/fr/nlu).

## J'ai quelques questions!

> Pourquoi ne prenez-vous que les 3 premiers résultats et pas plus ?

C'est un délicat équilibre entre le temps qu'il faut pour répondre à une question et l'exactitude de la réponse.

> Pourquoi faut-il autant de temps pour répondre ?

Vous devez comprendre ce que le domaine fait ici. Répondre à une question complexe en fonction d'un élément de contexte pertinent est un processus de calcul intensif néanmoins nécessaire pour trouver la réponse que vous recherchez.

Le domaine est configuré de manière à fournir une réponse en moins de 10 secondes (de manière optimale). Si cela reste encore trop long, vous pouvez réduire la taille du contexte (de 3 à 1 page) et ignorer complètement le résumé des longues requêtes mais le calcul de la réponse prendra toujours un peu de temps.

> Comment puis-je améliorer les réponses que j'obtiens ?

Si vous souhaitez privilégier la précision à la rapidité, vous pouvez augmenter la taille du contexte (soit de 3 à 10 pages). Cependant, notez qu'un contexte trop large produira de mauvais résultats.

> Ce domaine fonctionne-t-il avec d'autres langues que le français?

Non, les domaines sont spécifiques à une langue. De plus, ce domaine s'appuie sur des actions qui utilisent des modèles pré-entraînés pour produire des réponses à partir du web. Ces modèles sont également spécifiques à une langue et ne peuvent donc pas être utilisés pour répondre à des requêtes dans une autre langue.

## Contributions

Ce domaine a été crée à l'aide du [modèle de domaine NLP](https://gitlab.com/waser-technologies/cookiecutters/nlu-domain-template).

```zsh
dmt --create
```